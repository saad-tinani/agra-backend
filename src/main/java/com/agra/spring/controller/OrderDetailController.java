package com.agra.spring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.agra.spring.model.OrderDetail;
import com.agra.spring.service.OrderDetailService;

@RestController
@CrossOrigin
public class OrderDetailController {

	@Autowired
	private OrderDetailService orderDetailService;

	// Get all the orders details
	@GetMapping("/api/orders_items/{id}")
	public ResponseEntity<List<OrderDetail>> list(@PathVariable("id") long id) {
		List<OrderDetail> list = orderDetailService.list(id);
		return ResponseEntity.ok().body(list);
	}

	// Get a single order detail by id
	@GetMapping("/api/orders_details/{id}")
	public ResponseEntity<OrderDetail> get(@PathVariable("id") long id) {
		OrderDetail order = orderDetailService.get(id);
		return ResponseEntity.ok().body(order);
	}

	// Save an order detail
	@PostMapping("/api/orders_details")
	public ResponseEntity<?> save(@RequestBody OrderDetail[] order) {
		return ResponseEntity.ok().body(orderDetailService.save(order));
	}

	// Update an order detail
	@PutMapping("/api/orders_details/{id}")
	public ResponseEntity<?> update(@PathVariable("id") long id, @RequestBody OrderDetail order) {
		orderDetailService.update(id, order);
		return ResponseEntity.ok().body("OrderDetail has been updated");
	}
	
	// Update order details
	@PutMapping("/api/orders_items")
	public ResponseEntity<?> updateOrderDetails(@RequestBody OrderDetail[] orderDetails) {
		for (OrderDetail orderDetail : orderDetails) {
			orderDetailService.update(orderDetail.getId(), orderDetail);
		}
		return ResponseEntity.ok().body("OrderDetail has been updated");
	}
	
	

	// Delete an order detail
	@DeleteMapping("/api/orders_details/{id}")
	public ResponseEntity<?> delete(@PathVariable("id") long id) {
		orderDetailService.delete(id);
		return ResponseEntity.ok().body("OrderDetail has been deleted");
	}

}
