package com.agra.spring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.agra.spring.model.Region;
import com.agra.spring.service.RegionService;

@RestController
@CrossOrigin
public class RegionController {

	@Autowired
	private RegionService regionService;

	// Get all the regions
	@GetMapping("/api/regions")
	public ResponseEntity<List<Region>> list() {
		List<Region> list = regionService.list();
		return ResponseEntity.ok().body(list);
	}

	// Get a single region by id
	@GetMapping("/api/regions/{id}")
	public ResponseEntity<Region> get(@PathVariable("id") long id) {
		Region region = regionService.get(id);
		return ResponseEntity.ok().body(region);
	}

}
