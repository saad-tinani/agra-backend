package com.agra.spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.agra.spring.model.Director;
import com.agra.spring.model.Engineer;
import com.agra.spring.model.User;
import com.agra.spring.service.AuthService;

@RestController
@CrossOrigin
public class AuthController {
	
	@Autowired
	private AuthService authService;

	// Sign-in
	@PostMapping("/api/auth/signin")
	public ResponseEntity<User> login(@RequestBody User user) {
		return ResponseEntity.ok().body(authService.login(user));
	}
	
	// Get Engineer
	@GetMapping("/api/auth/engineers/{user_id}")
	public ResponseEntity<Engineer> getEngineer(@PathVariable("user_id") long user_id) {
		return ResponseEntity.ok().body(authService.getEngineer(user_id));
	}
	
	// Get Director
	@GetMapping("/api/auth/directors/{user_id}")
	public ResponseEntity<Director> getDirector(@PathVariable("user_id") long user_id) {
		return ResponseEntity.ok().body(authService.getDirector(user_id));
	}

}
