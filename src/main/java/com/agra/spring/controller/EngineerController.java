package com.agra.spring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.agra.spring.model.Engineer;
import com.agra.spring.service.EngineerService;

@RestController
@CrossOrigin
public class EngineerController {

	@Autowired
	private EngineerService engineerService;

	// Get all the engineers
	@GetMapping("/api/engineers")
	public ResponseEntity<List<Engineer>> list() {
		List<Engineer> list = engineerService.list();
		return ResponseEntity.ok().body(list);
	}

	// Get all the engineers of a region
	@PostMapping("/api/directorEngineers")
	public ResponseEntity<List<Engineer>> engineers(@RequestBody long region_id) {
		List<Engineer> list = engineerService.engineers(region_id);
		return ResponseEntity.ok().body(list);
	}

	// Get a single engineer by id
	@GetMapping("/api/engineers/{id}")
	public ResponseEntity<Engineer> get(@PathVariable("id") long id) {
		Engineer engineer = engineerService.get(id);
		return ResponseEntity.ok().body(engineer);
	}

	// Save an engineer
	@PostMapping("/api/engineers")
	public ResponseEntity<?> save(@RequestBody Engineer engineer) {
		return ResponseEntity.ok().body(engineerService.save(engineer));
	}

	// Update a engineer
	@PutMapping("/api/engineers/{id}")
	public ResponseEntity<?> update(@PathVariable("id") long id, @RequestBody Engineer engineer) {
		engineerService.update(id, engineer);
		return ResponseEntity.ok().body("Engineer has been updated");
	}

	// Delete an engineer
	@DeleteMapping("/api/engineers/{id}")
	public ResponseEntity<?> delete(@PathVariable("id") long id) {
		engineerService.delete(id);
		return ResponseEntity.ok().body("Engineer has been deleted");
	}

}
