package com.agra.spring.controller;

import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.agra.spring.model.Category;
import com.agra.spring.service.CategoryService;

@RestController
@CrossOrigin
public class CategoryController {

	@Autowired
	private CategoryService categoryService;

	// Get all the categories
	@GetMapping("/api/categories")
	public ResponseEntity<List<Category>> list() {
		List<Category> list = categoryService.list();
		return ResponseEntity.ok().body(list);
	}

	// Get a single order by id
	@GetMapping("/api/categories/{id}")
	public ResponseEntity<Category> get(@PathVariable("id") long id) {
		Category order = categoryService.get(id);
		return ResponseEntity.ok().body(order);
	}

	// Save an order
	@PostMapping("/api/categories")
	public ResponseEntity<?> save(@RequestBody Category order) {
		try {
			return ResponseEntity.ok().body(categoryService.save(order));
		} catch (ConstraintViolationException e) {
			return ResponseEntity.ok().body("An order with the same reference already exists !");
		}

	}

	// Update an order
	@PutMapping("/api/categories/{id}")
	public ResponseEntity<?> update(@PathVariable("id") long id, @RequestBody Category order) {
		categoryService.update(id, order);
		return ResponseEntity.ok().body("Category has been updated");
	}

	// Delete an order
	@DeleteMapping("/api/categories/{id}")
	public ResponseEntity<?> delete(@PathVariable("id") long id) {
		categoryService.delete(id);
		return ResponseEntity.ok().body("Category has been deleted");
	}

}
