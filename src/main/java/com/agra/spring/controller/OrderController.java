package com.agra.spring.controller;

import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.agra.spring.model.Order;
import com.agra.spring.service.OrderService;

@RestController
@CrossOrigin
public class OrderController {

	@Autowired
	private OrderService orderService;

	// Get all the orders
	@GetMapping("/api/orders")
	public ResponseEntity<List<Order>> list() {
		List<Order> list = orderService.list();
		return ResponseEntity.ok().body(list);
	}

	// Get all the orders of a region
	@PostMapping("/api/regionOrders")
	public ResponseEntity<List<Order>> list(@RequestBody long region_id) {
		List<Order> list = orderService.listByRegion(region_id);
		return ResponseEntity.ok().body(list);
	}

	// Get a single order by id
	@GetMapping("/api/orders/{id}")
	public ResponseEntity<Order> get(@PathVariable("id") long id) {
		Order order = orderService.get(id);
		return ResponseEntity.ok().body(order);
	}

	// Save an order
	@PostMapping("/api/orders")
	public ResponseEntity<?> save(@RequestBody Order order) {
		try {
			return ResponseEntity.ok().body(orderService.save(order));
		} catch (ConstraintViolationException e) {
			return ResponseEntity.ok().body("An order with the same reference already exists !");
		}

	}

	// Update an order
	@PutMapping("/api/orders/{id}")
	public ResponseEntity<?> update(@PathVariable("id") long id, @RequestBody Order order) {
		orderService.update(id, order);
		return ResponseEntity.ok().body("Order has been updated");
	}

	// Delete an order
	@DeleteMapping("/api/orders/{id}")
	public ResponseEntity<?> delete(@PathVariable("id") long id) {
		orderService.delete(id);
		return ResponseEntity.ok().body("Order has been deleted");
	}

}
