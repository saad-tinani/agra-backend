package com.agra.spring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.agra.spring.model.Role;
import com.agra.spring.service.RoleService;

@RestController
@CrossOrigin
public class RoleController {
	
	@Autowired
	private RoleService roleService;
	
	// Get all the roles
	@GetMapping("/api/roles")
	public ResponseEntity<List<Role>> list() {
		List<Role> list = roleService.list();
		return ResponseEntity.ok().body(list);
	}
	
	// Get a single role by id
	@GetMapping("/api/roles/{id}")
	public ResponseEntity<Role> get(@PathVariable("id") long id) {
		Role role = roleService.get(id);
		return ResponseEntity.ok().body(role);
	}
	
	// Save a role
	@PostMapping("/api/roles")
	public ResponseEntity<?> save(@RequestBody Role role) {
		return ResponseEntity.ok().body(roleService.save(role));
	}
	
	// Update a role
	@PutMapping("/api/roles/{id}")
	public ResponseEntity<?> update(@PathVariable("id") long id, @RequestBody Role role) {
		roleService.update(id, role);
		return ResponseEntity.ok().body("Role has been updated");
	}
	
	// Delete a role
	@DeleteMapping("/api/roles/{id}")
	public ResponseEntity<?> delete(@PathVariable("id") long id) {
		roleService.delete(id);
		return ResponseEntity.ok().body("Role has been deleted");
	}

}
