package com.agra.spring.controller;

import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.agra.spring.model.User;
import com.agra.spring.service.UserService;

@RestController
@CrossOrigin
// @CrossOrigin(origins = "http://example.com", maxAge = 3600)
public class UserController {

	@Autowired
	private UserService userService;

	// Get all the users
	@GetMapping("/api/users")
	public ResponseEntity<List<User>> list() {
		List<User> list = userService.list();
		return ResponseEntity.ok().body(list);
	}

	// Get all the clients
	@GetMapping("/api/clients")
	public ResponseEntity<List<User>> clients() {
		List<User> list = userService.clients();
		return ResponseEntity.ok().body(list);
	}

	// Get all the clients of an engineer
	@PostMapping("/api/engineerClients")
	public ResponseEntity<List<User>> clients(@RequestBody Integer[] cities_ids) {
		List<User> list = userService.clients(cities_ids);
		return ResponseEntity.ok().body(list);
	}

	// Get all the clients of a director
	@PostMapping("/api/directorClients")
	public ResponseEntity<List<User>> clients(@RequestBody long work_region_id) {
		List<User> list = userService.clients(work_region_id);
		return ResponseEntity.ok().body(list);
	}

	// Get a single user by id
	@GetMapping("/api/users/{id}")
	public ResponseEntity<User> get(@PathVariable("id") long id) {
		User user = userService.get(id);
		return ResponseEntity.ok().body(user);
	}

	// Check if a user exists based on his username
	@PostMapping("/api/users/checkUsername")
	public ResponseEntity<Boolean> checkUsername(@RequestBody String username) {
		return ResponseEntity.ok().body(userService.checkUsername(username));
	}

	// Check if a user exists based on his email
	@PostMapping("/api/users/checkEmail")
	public ResponseEntity<Boolean> checkEmail(@RequestBody String email) {
		return ResponseEntity.ok().body(userService.checkEmail(email));
	}

	// Check if a user exists based on his CIN
	@PostMapping("/api/users/checkCin")
	public ResponseEntity<Boolean> get(@RequestBody String cin) {
		return ResponseEntity.ok().body(userService.checkCin(cin));
	}

	// Save a user
	@PostMapping("/api/users")
	public ResponseEntity<?> save(@RequestBody User user) {
		try {
			return ResponseEntity.ok().body(userService.save(user));
		} catch (ConstraintViolationException e) {
			return ResponseEntity.ok().body("A user with the same infos already exists !");
		}

	}

	// Update a user
	@PutMapping("/api/users/{id}")
	public ResponseEntity<?> update(@PathVariable("id") long id, @RequestBody User user) {
		userService.update(id, user);
		return ResponseEntity.ok().body("User has been updated");
	}

	// Delete a user
	@DeleteMapping("/api/users/{id}")
	public ResponseEntity<?> delete(@PathVariable("id") long id) {
		userService.delete(id);
		return ResponseEntity.ok().body("User has been deleted");
	}

}
