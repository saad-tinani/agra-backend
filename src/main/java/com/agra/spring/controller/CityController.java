package com.agra.spring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.agra.spring.model.City;
import com.agra.spring.service.CityService;

@RestController
@CrossOrigin
public class CityController {

	@Autowired
	private CityService cityService;

	// Get all the cities
	@GetMapping("/api/cities")
	public ResponseEntity<List<City>> list() {
		List<City> list = cityService.list();
		return ResponseEntity.ok().body(list);
	}

	// Get all the cities of a specific region
	@GetMapping("/api/workcities/{region_id}")
	public ResponseEntity<List<City>> workCities(@PathVariable("region_id") long region_id) {
		List<City> list = cityService.workCities(region_id);
		return ResponseEntity.ok().body(list);
	}

	// Get a single city by id
	@GetMapping("/api/cities/{id}")
	public ResponseEntity<City> get(@PathVariable("id") long id) {
		City city = cityService.get(id);
		return ResponseEntity.ok().body(city);
	}

}
