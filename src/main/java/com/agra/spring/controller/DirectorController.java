package com.agra.spring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.agra.spring.model.Director;
import com.agra.spring.service.DirectorService;

@RestController
@CrossOrigin
public class DirectorController {

	@Autowired
	private DirectorService directorService;

	// Get all the directors
	@GetMapping("/api/directors")
	public ResponseEntity<List<Director>> list() {
		List<Director> list = directorService.list();
		return ResponseEntity.ok().body(list);
	}

	// Get a single director by id
	@GetMapping("/api/directors/{id}")
	public ResponseEntity<Director> get(@PathVariable("id") long id) {
		Director director = directorService.get(id);
		return ResponseEntity.ok().body(director);
	}

	// Save a director
	@PostMapping("/api/directors")
	public ResponseEntity<?> save(@RequestBody Director director) {
		return ResponseEntity.ok().body(directorService.save(director));
	}

	// Update a director
	@PutMapping("/api/directors/{id}")
	public ResponseEntity<?> update(@PathVariable("id") long id, @RequestBody Director director) {
		directorService.update(id, director);
		return ResponseEntity.ok().body("Director has been updated");
	}

	// Delete a director
	@DeleteMapping("/api/directors/{id}")
	public ResponseEntity<?> delete(@PathVariable("id") long id) {
		directorService.delete(id);
		return ResponseEntity.ok().body("Director has been deleted");
	}

}
