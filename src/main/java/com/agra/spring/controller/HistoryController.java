package com.agra.spring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.agra.spring.model.History;
import com.agra.spring.service.HistoryService;

@RestController
@CrossOrigin
public class HistoryController {

	@Autowired
	private HistoryService historyService;

	// Get all the history
	@GetMapping("/api/history")
	public ResponseEntity<List<History>> list() {
		List<History> list = historyService.list();
		return ResponseEntity.ok().body(list);
	}

	// Get a single history by id
	@GetMapping("/api/history/{id}")
	public ResponseEntity<History> get(@PathVariable("id") long id) {
		History history = historyService.get(id);
		return ResponseEntity.ok().body(history);
	}

	// Save a history
	@PostMapping("/api/history")
	public ResponseEntity<?> save(@RequestBody History history) {
		return ResponseEntity.ok().body(historyService.save(history));
	}

}
