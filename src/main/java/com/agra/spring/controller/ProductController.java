package com.agra.spring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.agra.spring.model.Product;
import com.agra.spring.service.ProductService;

@RestController
@CrossOrigin
public class ProductController {

	@Autowired
	private ProductService productService;

	// Get all the products
	@GetMapping("/api/products")
	public ResponseEntity<List<Product>> list() {
		List<Product> list = productService.list();
		return ResponseEntity.ok().body(list);
	}

	// Get all the products of a region
	@PostMapping("/api/regionProducts")
	public ResponseEntity<List<Product>> products(@RequestBody int region_id) {
		List<Product> list = productService.products(region_id);
		return ResponseEntity.ok().body(list);
	}

	// Get a single product by id
	@GetMapping("/api/products/{id}")
	public ResponseEntity<Product> get(@PathVariable("id") long id) {
		Product product = productService.get(id);
		return ResponseEntity.ok().body(product);
	}

	// Get category products
	@GetMapping("/api/categoryProducts/{id}")
	public ResponseEntity<List<Product>> getCategoryProducts(@PathVariable("id") long id) {
		List<Product> list = productService.getCategoryProducts(id);
		return ResponseEntity.ok().body(list);
	}
		
	// Save a product
	@PostMapping("/api/products")
	public ResponseEntity<?> save(@RequestBody Product product) {
		return ResponseEntity.ok().body(productService.save(product));
	}

	// Update a product
	@PutMapping("/api/products/{id}")
	public ResponseEntity<?> update(@PathVariable("id") long id, @RequestBody Product product) {
		productService.update(id, product);
		return ResponseEntity.ok().body("Product has been updated");
	}

	// Delete a product
	@DeleteMapping("/api/products/{id}")
	public ResponseEntity<?> delete(@PathVariable("id") long id) {
		productService.delete(id);
		return ResponseEntity.ok().body("Product has been deleted");
	}

}
