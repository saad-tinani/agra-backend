package com.agra.spring.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.agra.spring.dao.RegionDAO;
import com.agra.spring.model.Region;

@Service
public class RegionServiceImpl implements RegionService {
	
	@Autowired
	private RegionDAO regionDAO;

	@Override
	@Transactional
	public Region get(long id) {
		return regionDAO.get(id);
	}

	@Override
	@Transactional
	public List<Region> list() {
		return regionDAO.list();
	}

}
