package com.agra.spring.service;

import java.util.List;

import com.agra.spring.model.Director;

public interface DirectorService {

	// Save the record
	Director save(Director director);

	// Get a single record
	Director get(long id);

	// Get all the records
	List<Director> list();

	// Update a record
	void update(long id, Director director);

	// Delete a record
	void delete(long id);

}
