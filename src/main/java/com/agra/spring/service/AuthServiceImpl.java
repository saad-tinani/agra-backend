package com.agra.spring.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.agra.spring.dao.AuthDAO;
import com.agra.spring.model.Director;
import com.agra.spring.model.Engineer;
import com.agra.spring.model.User;

@Service
public class AuthServiceImpl implements AuthService {
	
	@Autowired
	private AuthDAO authDAO;

	@Override
	@Transactional
	public User login(User user) {
		return authDAO.login(user);
	}

	@Override
	@Transactional
	public Engineer getEngineer(long user_id) {
		return authDAO.getEngineer(user_id);
	}

	@Override
	@Transactional
	public Director getDirector(long user_id) {
		return authDAO.getDirector(user_id);
	}

}
