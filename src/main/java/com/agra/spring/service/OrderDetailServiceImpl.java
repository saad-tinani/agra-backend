package com.agra.spring.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.agra.spring.dao.OrderDetailDAO;
import com.agra.spring.model.OrderDetail;

@Service
public class OrderDetailServiceImpl implements OrderDetailService {

	@Autowired
	private OrderDetailDAO orderDetailDAO;

	@Override
	@Transactional
	public OrderDetail[] save(OrderDetail[] orderDetails) {
		return orderDetailDAO.save(orderDetails);
	}

	@Override
	@Transactional
	public OrderDetail get(long id) {
		return orderDetailDAO.get(id);
	}

	@Override
	@Transactional
	public List<OrderDetail> list(long id) {
		return orderDetailDAO.list(id);
	}

	@Override
	@Transactional
	public void update(long id, OrderDetail orderDetail) {
		orderDetailDAO.update(id, orderDetail);
	}

	@Override
	@Transactional
	public void delete(long id) {
		orderDetailDAO.delete(id);
	}

}
