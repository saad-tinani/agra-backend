package com.agra.spring.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.agra.spring.dao.HistoryDAO;
import com.agra.spring.model.History;

@Service
public class HistoryServiceImpl implements HistoryService {
	
	@Autowired
	private HistoryDAO historyDAO;

	@Override
	@Transactional
	public History save(History history) {
		return historyDAO.save(history);
	}

	@Override
	@Transactional
	public History get(long id) {
		return historyDAO.get(id);
	}

	@Override
	@Transactional
	public List<History> list() {
		return historyDAO.list();
	}

}
