package com.agra.spring.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.agra.spring.dao.ProductDAO;
import com.agra.spring.model.Product;

@Service
public class ProductServiceImpl implements ProductService {
	
	@Autowired
	private ProductDAO productDAO;

	@Override
	@Transactional
	public Product save(Product product) {
		return productDAO.save(product);
	}

	@Override
	@Transactional
	public Product get(long id) {
		return productDAO.get(id);
	}

	@Override
	@Transactional
	public List<Product> list() {
		return productDAO.list();
	}

	@Override
	@Transactional
	public List<Product> products(long region_id) {
		return productDAO.products(region_id);
	}

	@Override
	@Transactional
	public void update(long id, Product product) {
		productDAO.update(id, product);
	}

	@Override
	@Transactional
	public void delete(long id) {
		productDAO.delete(id);
	}

	@Override
	@Transactional
	public List<Product> getCategoryProducts(long category_id) {
		return productDAO.getCategoryProducts(category_id);
	}

}
