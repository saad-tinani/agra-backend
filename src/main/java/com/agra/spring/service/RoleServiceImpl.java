package com.agra.spring.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.agra.spring.dao.RoleDAO;
import com.agra.spring.model.Role;

@Service
public class RoleServiceImpl implements RoleService {
	
	@Autowired
	private RoleDAO roleDAO;

	@Override
	@Transactional
	public Role save(Role role) {
		return roleDAO.save(role);
	}

	@Override
	@Transactional
	public Role get(long id) {
		return roleDAO.get(id);
	}

	@Override
	@Transactional
	public List<Role> list() {
		return roleDAO.list();
	}

	@Override
	@Transactional
	public void update(long id, Role role) {
		roleDAO.update(id, role);
	}

	@Override
	@Transactional
	public void delete(long id) {
		roleDAO.delete(id);
	}
	
	

}
