package com.agra.spring.service;

import java.util.List;

import com.agra.spring.model.Category;

public interface CategoryService {

	// Save the record
	Category save(Category category);

	// Get a single record
	Category get(long id);

	// Get all the records
	List<Category> list();

	// Update a record
	void update(long id, Category category);

	// Delete a record
	void delete(long id);

}
