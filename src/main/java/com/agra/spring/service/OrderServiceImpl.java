package com.agra.spring.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.agra.spring.dao.OrderDAO;
import com.agra.spring.model.Order;

@Service
public class OrderServiceImpl implements OrderService {
	
	@Autowired
	private OrderDAO orderDAO;

	@Override
	@Transactional
	public Order save(Order order) {
		return orderDAO.save(order);
	}

	@Override
	@Transactional
	public Order get(long id) {
		return orderDAO.get(id);
	}

	@Override
	@Transactional
	public List<Order> list() {
		return orderDAO.list();
	}

	@Override
	@Transactional
	public List<Order> listByRegion(long region_id) {
		return orderDAO.listByRegion(region_id);
	}

	@Override
	@Transactional
	public void update(long id, Order order) {
		orderDAO.update(id, order);
	}

	@Override
	@Transactional
	public void delete(long id) {
		orderDAO.delete(id);
	}

}
