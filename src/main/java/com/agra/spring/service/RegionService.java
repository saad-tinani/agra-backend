package com.agra.spring.service;

import java.util.List;

import com.agra.spring.model.Region;

public interface RegionService {

	// Get a single record
	Region get(long id);

	// Get all the records
	List<Region> list();

}
