package com.agra.spring.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.agra.spring.dao.DirectorDAO;
import com.agra.spring.model.Director;

@Service
public class DirectorServiceImpl implements DirectorService {
	
	@Autowired
	private DirectorDAO directorDAO;

	@Override
	@Transactional
	public Director save(Director director) {
		return directorDAO.save(director);
	}

	@Override
	@Transactional
	public Director get(long id) {
		return directorDAO.get(id);
	}

	@Override
	@Transactional
	public List<Director> list() {
		return directorDAO.list();
	}

	@Override
	@Transactional
	public void update(long id, Director director) {
		directorDAO.update(id, director);
	}

	@Override
	@Transactional
	public void delete(long id) {
		directorDAO.delete(id);
	}
	
	

}
