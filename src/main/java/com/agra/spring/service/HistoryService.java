package com.agra.spring.service;

import java.util.List;

import com.agra.spring.model.History;

public interface HistoryService {

	// Save the record
	History save(History history);

	// Get a single record
	History get(long id);

	// Get all the records
	List<History> list();

}
