package com.agra.spring.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.agra.spring.dao.CategoryDAO;
import com.agra.spring.model.Category;

@Service
public class CategoryServiceImpl implements CategoryService {
	
	@Autowired
	private CategoryDAO categoryDAO;

	@Override
	@Transactional
	public Category save(Category category) {
		return categoryDAO.save(category);
	}

	@Override
	@Transactional
	public Category get(long id) {
		return categoryDAO.get(id);
	}

	@Override
	@Transactional
	public List<Category> list() {
		return categoryDAO.list();
	}

	@Override
	@Transactional
	public void update(long id, Category category) {
		categoryDAO.update(id, category);
	}

	@Override
	@Transactional
	public void delete(long id) {
		categoryDAO.delete(id);
	}

}
