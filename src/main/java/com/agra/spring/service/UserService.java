package com.agra.spring.service;

import java.util.List;

import com.agra.spring.model.User;

public interface UserService {

	// Save the record
	User save(User user);

	// Get a single record
	User get(long id);

	// Get all the records
	List<User> list();

	// Get all the clients
	List<User> clients();

	// Get the engineer's clients
	List<User> clients(Integer[] cities_ids);

	// Get the director's clients
	List<User> clients(long work_region_id);

	// Check if a user exists based on his username
	boolean checkUsername(String username);

	// Check if a user exists based on his email
	boolean checkEmail(String email);

	// Check if a user exists based on his CIN
	boolean checkCin(String cin);

	// Update a record
	void update(long id, User user);

	// Delete a record
	void delete(long id);

}
