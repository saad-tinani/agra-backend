package com.agra.spring.service;

import java.util.List;

import com.agra.spring.model.Product;

public interface ProductService {

	// Save the record
	Product save(Product product);

	// Get a single record
	Product get(long id);

	// Get all the records
	List<Product> list();

	// Get all the products of a region
	List<Product> products(long region_id);

	// Update a record
	void update(long id, Product product);

	// Delete a record
	void delete(long id);
	
	// Get categorie products
	List<Product> getCategoryProducts(long category_id);

}
