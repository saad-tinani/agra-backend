package com.agra.spring.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.agra.spring.dao.CityDAO;
import com.agra.spring.model.City;

@Service
public class CityServiceImpl implements CityService {
	
	@Autowired
	private CityDAO cityDAO;

	@Override
	@Transactional
	public City get(long id) {
		return cityDAO.get(id);
	}

	@Override
	@Transactional
	public List<City> list() {
		return cityDAO.list();
	}

	@Override
	@Transactional
	public List<City> workCities(long region_id) {
		return cityDAO.workCities(region_id);
	}

}
