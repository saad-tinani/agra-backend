package com.agra.spring.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.agra.spring.dao.EngineerDAO;
import com.agra.spring.model.Engineer;

@Service
public class EngineerServiceImpl implements EngineerService {
	
	@Autowired
	private EngineerDAO engineerDAO;

	@Override
	@Transactional
	public Engineer save(Engineer engineer) {
		return engineerDAO.save(engineer);
	}

	@Override
	@Transactional
	public Engineer get(long id) {
		return engineerDAO.get(id);
	}

	@Override
	@Transactional
	public List<Engineer> list() {
		return engineerDAO.list();
	}
	
	@Override
	@Transactional
	public List<Engineer> engineers(long region_id) {
		return engineerDAO.engineers(region_id);
	}

	@Override
	@Transactional
	public void update(long id, Engineer engineer) {
		engineerDAO.update(id, engineer);
	}

	@Override
	@Transactional
	public void delete(long id) {
		engineerDAO.delete(id);
	}

}
