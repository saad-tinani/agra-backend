package com.agra.spring.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.agra.spring.dao.UserDAO;
import com.agra.spring.model.User;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDAO userDAO;

	@Override
	@Transactional
	public User save(User user) {
		return userDAO.save(user);
	}

	@Override
	@Transactional
	public User get(long id) {
		return userDAO.get(id);
	}

	@Override
	@Transactional
	public List<User> list() {
		return userDAO.list();
	}

	@Override
	@Transactional
	public List<User> clients() {
		return userDAO.clients();
	}
	
	@Override
	@Transactional
	public List<User> clients(Integer[] cities_ids) {
		return userDAO.clients(cities_ids);
	}
	
	@Override
	@Transactional
	public List<User> clients(long work_region_id) {
		return userDAO.clients(work_region_id);
	}

	@Override
	@Transactional
	public void update(long id, User user) {
		userDAO.update(id, user);
	}

	@Override
	@Transactional
	public void delete(long id) {
		userDAO.delete(id);
	}

	@Override
	@Transactional
	public boolean checkUsername(String username) {
		return userDAO.checkUsername(username);
	}

	@Override
	@Transactional
	public boolean checkEmail(String email) {
		return userDAO.checkEmail(email);
	}

	@Override
	@Transactional
	public boolean checkCin(String cin) {
		return userDAO.checkCin(cin);
	}

}
