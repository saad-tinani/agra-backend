package com.agra.spring.service;

import java.util.List;

import com.agra.spring.model.Order;

public interface OrderService {

	// Save the record
	Order save(Order order);

	// Get a single record
	Order get(long id);

	// Get all the records
	List<Order> list();

	// Get all the records of a region
	List<Order> listByRegion(long region_id);

	// Update a record
	void update(long id, Order order);

	// Delete a record
	void delete(long id);

}
