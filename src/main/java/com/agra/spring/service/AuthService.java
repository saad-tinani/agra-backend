package com.agra.spring.service;

import com.agra.spring.model.Director;
import com.agra.spring.model.Engineer;
import com.agra.spring.model.User;

public interface AuthService {
	
	// Check User
	User login(User user);
		
	// Get Engineer
	Engineer getEngineer(long user_id);
	
	// Get Director
	Director getDirector(long user_id);

}
