package com.agra.spring.model;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

@Entity
@Table(name = "roles")
public class Role {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String name;
	private boolean privileges_management;
	private boolean directors_management;
	private boolean engineers_management;
	private boolean clients_management;
	private boolean products_management;
	private boolean prices_management;
	private boolean sales_management;
	private boolean orders_management;
	@CreationTimestamp
	private Timestamp creation_date;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isPrivileges_management() {
		return privileges_management;
	}

	public void setPrivileges_management(boolean privileges_management) {
		this.privileges_management = privileges_management;
	}

	public boolean isDirectors_management() {
		return directors_management;
	}

	public void setDirectors_management(boolean directors_management) {
		this.directors_management = directors_management;
	}

	public boolean isEngineers_management() {
		return engineers_management;
	}

	public void setEngineers_management(boolean engineers_management) {
		this.engineers_management = engineers_management;
	}

	public boolean isClients_management() {
		return clients_management;
	}

	public void setClients_management(boolean clients_management) {
		this.clients_management = clients_management;
	}

	public boolean isProducts_management() {
		return products_management;
	}

	public void setProducts_management(boolean products_management) {
		this.products_management = products_management;
	}

	public boolean isPrices_management() {
		return prices_management;
	}

	public void setPrices_management(boolean prices_management) {
		this.prices_management = prices_management;
	}

	public boolean isSales_management() {
		return sales_management;
	}

	public void setSales_management(boolean sales_management) {
		this.sales_management = sales_management;
	}

	public boolean isOrders_management() {
		return orders_management;
	}

	public void setOrders_management(boolean orders_management) {
		this.orders_management = orders_management;
	}

	public Timestamp getCreation_date() {
		return creation_date;
	}

	public void setCreation_date(Timestamp creation_date) {
		this.creation_date = creation_date;
	}

	@Override
	public String toString() {
		return "Role [id=" + id + ", name=" + name + ", privileges_management=" + privileges_management
				+ ", directors_management=" + directors_management + ", engineers_management=" + engineers_management
				+ ", clients_management=" + clients_management + ", products_management=" + products_management
				+ ", prices_management=" + prices_management + ", sales_management=" + sales_management
				+ ", orders_management=" + orders_management + ", creation_date=" + creation_date + "]";
	}
	
}
