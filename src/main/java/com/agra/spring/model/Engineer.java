package com.agra.spring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "engineers")
public class Engineer {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@OneToOne
	@JoinColumn(name = "user_id")
	private User user;
	@ManyToOne
	@JoinColumn(name = "work_region_id")
	private Region work_region;
	private String work_cities_ids;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Region getWork_region() {
		return work_region;
	}

	public void setWork_region(Region work_region) {
		this.work_region = work_region;
	}

	public String getWork_cities_ids() {
		return work_cities_ids;
	}

	public void setWork_cities_ids(String work_cities_ids) {
		this.work_cities_ids = work_cities_ids;
	}

	@Override
	public String toString() {
		return "Engineer [id=" + id + ", user=" + user + ", work_region=" + work_region + ", work_cities_ids="
				+ work_cities_ids + "]";
	}

}
