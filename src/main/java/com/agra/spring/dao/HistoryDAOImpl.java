package com.agra.spring.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.agra.spring.model.History;

@Repository
public class HistoryDAOImpl implements HistoryDAO {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public History save(History history) {
		sessionFactory.getCurrentSession().save(history);
		return history;
	}

	@Override
	public History get(long id) {
		return sessionFactory.getCurrentSession().get(History.class, id);
	}

	@Override
	public List<History> list() {
		List<History> list = sessionFactory.getCurrentSession().createQuery("FROM com.agra.spring.model.History").list();
		return list;
	}

}
