package com.agra.spring.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.agra.spring.model.City;

@Repository
public class CityDAOImpl implements CityDAO {
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public City get(long id) {
		return sessionFactory.getCurrentSession().get(City.class, id);
	}

	@Override
	public List<City> list() {
		List<City> list = sessionFactory.getCurrentSession().createQuery("FROM com.agra.spring.model.City").list();
		return list;
	}

	@Override
	public List<City> workCities(long region_id) {
		List<City> list = sessionFactory.getCurrentSession().createQuery("FROM com.agra.spring.model.City WHERE region_id =" + region_id).list();
		return list;
	}

}
