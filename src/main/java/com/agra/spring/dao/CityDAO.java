package com.agra.spring.dao;

import java.util.List;

import com.agra.spring.model.City;

public interface CityDAO {

	// Get a single record
	City get(long id);

	// Get all the records
	List<City> list();

	// Get all the cities of a specific region
	List<City> workCities(long region_id);

}
