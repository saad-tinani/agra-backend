package com.agra.spring.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.agra.spring.model.Director;

@Repository
public class DirectorDAOImpl implements DirectorDAO {
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Director save(Director director) {
		sessionFactory.getCurrentSession().save(director);
		return director;
	}

	@Override
	public Director get(long id) {
		return sessionFactory.getCurrentSession().get(Director.class, id);
	}

	@Override
	public List<Director> list() {
		List<Director> list = sessionFactory.getCurrentSession().createQuery("FROM com.agra.spring.model.Director").list();
		return list;
	}

	@Override
	public void update(long id, Director director) {
		Session session = sessionFactory.getCurrentSession();
		Director oldDirector = session.byId(Director.class).load(id);
		oldDirector.setUser(director.getUser());
		oldDirector.setWork_region(director.getWork_region());
		session.flush();
	}

	@Override
	public void delete(long id) {
		Session session = sessionFactory.getCurrentSession();
		Director director = session.byId(Director.class).load(id);
		session.delete(director);
	}
	
	

}
