package com.agra.spring.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.agra.spring.model.Region;

@Repository
public class RegionDAOImpl implements RegionDAO {
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Region get(long id) {
		return sessionFactory.getCurrentSession().get(Region.class, id);
	}

	@Override
	public List<Region> list() {
		List<Region> list = sessionFactory.getCurrentSession().createQuery("FROM com.agra.spring.model.Region").list();
		return list;
	}

}
