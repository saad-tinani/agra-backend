package com.agra.spring.dao;

import java.util.List;

import com.agra.spring.model.Engineer;

public interface EngineerDAO {

	// Save the record
	Engineer save(Engineer engineer);

	// Get a single record
	Engineer get(long id);

	// Get all the records
	List<Engineer> list();

	// Get all the records of a region
	List<Engineer> engineers(long region_id);

	// Update a record
	void update(long id, Engineer engineer);

	// Delete a record
	void delete(long id);

}
