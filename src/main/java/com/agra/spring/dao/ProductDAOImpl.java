package com.agra.spring.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.agra.spring.model.Product;

@Repository
public class ProductDAOImpl implements ProductDAO {
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Product save(Product product) {
		sessionFactory.getCurrentSession().save(product);
		return product;
	}

	@Override
	public Product get(long id) {
		return sessionFactory.getCurrentSession().get(Product.class, id);
	}

	@Override
	public List<Product> list() {
		List<Product> list = sessionFactory.getCurrentSession().createQuery("FROM com.agra.spring.model.Product").list();
		return list;
	}

	@Override
	public List<Product> products(long region_id) {
		List<Product> list = sessionFactory.getCurrentSession().createQuery("FROM com.agra.spring.model.Product WHERE region_id = '"+region_id+"'").list();
		return list;
	}

	@Override
	public void update(long id, Product product) {
		Session session = sessionFactory.getCurrentSession();
		Product oldProduct = session.byId(Product.class).load(id);
		oldProduct.setCategory(product.getCategory());
		oldProduct.setName(product.getName());
		oldProduct.setPicture(product.getPicture());
		oldProduct.setDescription(product.getDescription());
		oldProduct.setUnit_price(product.getUnit_price());
		oldProduct.setQuantity(product.getQuantity());
		oldProduct.setRegion(product.getRegion());
		oldProduct.setStatus(product.isStatus());
		session.flush();
	}

	@Override
	public void delete(long id) {
		Session session = sessionFactory.getCurrentSession();
		Product product = session.byId(Product.class).load(id);
		session.delete(product);
	}

	@Override
	public List<Product> getCategoryProducts(long category_id) {
		List<Product> list = sessionFactory.getCurrentSession().createQuery("FROM com.agra.spring.model.Product WHERE category_id = '"+category_id+"'").list();
		return list;
	}

}
