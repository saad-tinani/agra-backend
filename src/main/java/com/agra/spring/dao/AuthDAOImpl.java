package com.agra.spring.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.agra.spring.model.Director;
import com.agra.spring.model.Engineer;
import com.agra.spring.model.User;

@Repository
public class AuthDAOImpl implements AuthDAO {
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public User login(User user) {
		
		// Login query
		String query = "FROM com.agra.spring.model.User WHERE email = '"+user.getEmail()+"' AND password = '"+user.getPassword().hashCode()+"'";
		List<User> users = sessionFactory.getCurrentSession().createQuery(query).list();
		
		return users.size() > 0 ? users.get(0) : null;
		
	}

	@Override
	public Engineer getEngineer(long user_id) {
		// Get Engineer Query
		String query = "FROM com.agra.spring.model.Engineer WHERE user_id = '"+user_id+"'";
		List<Engineer> engineers = sessionFactory.getCurrentSession().createQuery(query).list();
		
		return engineers.size() > 0 ? engineers.get(0) : null;
	}

	@Override
	public Director getDirector(long user_id) {
		// Get Engineer Query
		String query = "FROM com.agra.spring.model.Director WHERE user_id = '"+user_id+"'";
		List<Director> directors = sessionFactory.getCurrentSession().createQuery(query).list();
		
		return directors.size() > 0 ? directors.get(0) : null;
	}

}
