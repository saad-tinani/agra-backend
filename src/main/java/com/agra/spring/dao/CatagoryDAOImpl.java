package com.agra.spring.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.agra.spring.model.Category;

@Repository
public class CatagoryDAOImpl implements CategoryDAO {
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Category save(Category category) {
		sessionFactory.getCurrentSession().save(category);
		return category;
	}

	@Override
	public Category get(long id) {
		return sessionFactory.getCurrentSession().get(Category.class, id);
	}

	@Override
	public List<Category> list() {
		List<Category> list = sessionFactory.getCurrentSession().createQuery("FROM com.agra.spring.model.Category").list();
		return list;
	}

	@Override
	public void update(long id, Category category) {
		Session session = sessionFactory.getCurrentSession();
		Category oldCategory = session.byId(Category.class).load(id);
		oldCategory.setName(category.getName());
		oldCategory.setDescription(category.getDescription());
		oldCategory.setPosition(category.getPosition());
		oldCategory.setStatus(category.isStatus());
		session.flush();
	}

	@Override
	public void delete(long id) {
		Session session = sessionFactory.getCurrentSession();
		Category category = session.byId(Category.class).load(id);
		session.delete(category);
	}

}
