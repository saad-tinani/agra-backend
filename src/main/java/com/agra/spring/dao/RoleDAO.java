package com.agra.spring.dao;

import java.util.List;

import com.agra.spring.model.Role;

public interface RoleDAO {
	
	// Save the record
	Role save(Role role);
	
	// Get a single record
	Role get(long id);
	
	// Get all the records
	List<Role> list();
	
	// Update a record
	void update(long id, Role role);
	
	// Delete a record
	void delete(long id);

}
