package com.agra.spring.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.agra.spring.model.OrderDetail;

@Repository
public class OrderDetailDAOImpl implements OrderDetailDAO {
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public OrderDetail[] save(OrderDetail[] orderDetails) {
		for (OrderDetail orderDetail : orderDetails) {
			sessionFactory.getCurrentSession().save(orderDetail);
		}
		return orderDetails;
	}

	@Override
	public OrderDetail get(long id) {
		return sessionFactory.getCurrentSession().get(OrderDetail.class, id);
	}

	@Override
	public List<OrderDetail> list(long id) {
		List<OrderDetail> list = sessionFactory.getCurrentSession().createQuery("FROM com.agra.spring.model.OrderDetail WHERE order_id = '"+id+"'").list();
		return list;
	}

	@Override
	public void update(long id, OrderDetail orderDetail) {
		Session session = sessionFactory.getCurrentSession();
		OrderDetail oldOrderDetail = session.byId(OrderDetail.class).load(id);
		oldOrderDetail.setOrder(orderDetail.getOrder());
		oldOrderDetail.setProduct(orderDetail.getProduct());
		oldOrderDetail.setNegociation_unit_price(orderDetail.getNegociation_unit_price());
		oldOrderDetail.setQuantity(orderDetail.getQuantity());
		session.flush();
	}

	@Override
	public void delete(long id) {
		Session session = sessionFactory.getCurrentSession();
		OrderDetail orderDetail = session.byId(OrderDetail.class).load(id);
		session.delete(orderDetail);
	}

}
