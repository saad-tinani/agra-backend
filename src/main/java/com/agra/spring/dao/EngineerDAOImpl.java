package com.agra.spring.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.agra.spring.model.Engineer;

@Repository
public class EngineerDAOImpl implements EngineerDAO {
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Engineer save(Engineer engineer) {
		sessionFactory.getCurrentSession().save(engineer);
		return engineer;
	}

	@Override
	public Engineer get(long id) {
		return sessionFactory.getCurrentSession().get(Engineer.class, id);
	}

	@Override
	public List<Engineer> list() {
		List<Engineer> list = sessionFactory.getCurrentSession().createQuery("FROM com.agra.spring.model.Engineer").list();
		return list;
	}
	
	@Override
	public List<Engineer> engineers(long region_id) {
		List<Engineer> list = sessionFactory.getCurrentSession().createQuery("FROM com.agra.spring.model.Engineer WHERE work_region = '"+region_id+"'").list();
		return list;
	}

	@Override
	public void update(long id, Engineer engineer) {
		Session session = sessionFactory.getCurrentSession();
		Engineer oldEngineer = session.byId(Engineer.class).load(id);
		oldEngineer.setUser(engineer.getUser());
		oldEngineer.setWork_region(engineer.getWork_region());
		oldEngineer.setWork_cities_ids(engineer.getWork_cities_ids());
		session.flush();
	}

	@Override
	public void delete(long id) {
		Session session = sessionFactory.getCurrentSession();
		Engineer engineer = session.byId(Engineer.class).load(id);
		session.delete(engineer);
	}

}
