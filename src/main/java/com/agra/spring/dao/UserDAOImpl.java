package com.agra.spring.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.agra.spring.model.User;

@Repository
public class UserDAOImpl implements UserDAO {
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public User save(User user) {
		user.setPassword(""+user.getPassword().hashCode());
		sessionFactory.getCurrentSession().save(user);
		return user;
	}

	@Override
	public User get(long id) {
		return sessionFactory.getCurrentSession().get(User.class, id);
	}

	@Override
	public List<User> list() {
		List<User> list = sessionFactory.getCurrentSession().createQuery("FROM com.agra.spring.model.User").list();
		return list;
	}
	
	@Override
	public List<User> clients() {
		List<User> list = sessionFactory.getCurrentSession().createQuery("FROM com.agra.spring.model.User WHERE role_id = 4").list();
		return list;
	}

	@Override
	public List<User> clients(Integer[] cities_ids) {
		List<User> list = new ArrayList<>();
		for (int i = 0; i < cities_ids.length; i++) {
			list.addAll(sessionFactory.getCurrentSession().createQuery("FROM com.agra.spring.model.User WHERE role_id = 4 AND city = '"+cities_ids[i]+"'").list());
		}
		return list;
	}
	
	@Override
	public List<User> clients(long work_region_id) {
		/*List<User> list = sessionFactory.getCurrentSession().createQuery("FROM com.agra.spring.model.User u INNER JOIN com.agra.spring.model.City c ON u.city = c.id WHERE u.role=4 AND c.region = '"+work_region_id+"'").list();
		return list;*/
		List<User> list = sessionFactory.getCurrentSession().createQuery("FROM com.agra.spring.model.User WHERE role_id = 4 AND city.region='"+work_region_id+"'").list();
		return list;
	}

	@Override
	public void update(long id, User user) {
		Session session = sessionFactory.getCurrentSession();
		User oldUser = session.byId(User.class).load(id);
		oldUser.setAvatar(user.getAvatar());
		oldUser.setUsername(user.getUsername());
		oldUser.setEmail(user.getEmail());
		oldUser.setPhone(user.getPhone());
		oldUser.setFirst_name(user.getFirst_name());
		oldUser.setLast_name(user.getLast_name());
		oldUser.setCin(user.getCin());
		oldUser.setAddress(user.getAddress());
		oldUser.setCity(user.getCity());
		oldUser.setZip_code(user.getZip_code());
		oldUser.setRole(user.getRole());
		oldUser.setStatus(user.isStatus());
		session.flush();
	}

	@Override
	public void delete(long id) {
		Session session = sessionFactory.getCurrentSession();
		User user = session.byId(User.class).load(id);
		session.delete(user);
	}

	@Override
	public boolean checkUsername(String username) {
		return sessionFactory.getCurrentSession().createQuery("FROM com.agra.spring.model.User u WHERE u.username = '" + username + "'").list().size() > 0;
	}

	@Override
	public boolean checkEmail(String email) {
		return sessionFactory.getCurrentSession().createQuery("FROM com.agra.spring.model.User WHERE email = '" + email + "'").list().size() > 0;
	}

	@Override
	public boolean checkCin(String cin) {
		return sessionFactory.getCurrentSession().createQuery("FROM com.agra.spring.model.User WHERE cin = '" + cin + "'").list().size() > 0;
	}

}
