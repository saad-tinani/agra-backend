package com.agra.spring.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.agra.spring.model.Order;

@Repository
public class OrderDAOImpl implements OrderDAO {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Order save(Order order) {
		sessionFactory.getCurrentSession().save(order);
		return order;
	}

	@Override
	public Order get(long id) {
		return sessionFactory.getCurrentSession().get(Order.class, id);
	}

	@Override
	public List<Order> list() {
		List<Order> list = sessionFactory.getCurrentSession().createQuery("FROM com.agra.spring.model.Order").list();
		return list;
	}

	@Override
	public List<Order> listByRegion(long region_id) {
		List<Order> list = sessionFactory.getCurrentSession().createQuery("FROM com.agra.spring.model.Order WHERE client.city.region ='"+region_id+"'").list();
		return list;
	}

	@Override
	public void update(long id, Order order) {
		Session session = sessionFactory.getCurrentSession();
		Order oldOrder = session.byId(Order.class).load(id);
		oldOrder.setReference(order.getReference());
		oldOrder.setClient(order.getClient());
		oldOrder.setStatus(order.getStatus());
		session.flush();
	}

	@Override
	public void delete(long id) {
		Session session = sessionFactory.getCurrentSession();
		Order order = session.byId(Order.class).load(id);
		session.delete(order);
	}

}
