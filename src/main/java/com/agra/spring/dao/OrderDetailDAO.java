package com.agra.spring.dao;

import java.util.List;

import com.agra.spring.model.OrderDetail;

public interface OrderDetailDAO {

	// Save the record
	OrderDetail[] save(OrderDetail[] orderDetails);

	// Get a single record
	OrderDetail get(long id);

	// Get all the records
	List<OrderDetail> list(long id);

	// Update a record
	void update(long id, OrderDetail orderDetail);

	// Delete a record
	void delete(long id);

}
