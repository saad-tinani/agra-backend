package com.agra.spring.dao;

import java.util.List;

import com.agra.spring.model.Region;

public interface RegionDAO {
		
	// Get a single record
	Region get(long id);
	
	// Get all the records
	List<Region> list();

}
