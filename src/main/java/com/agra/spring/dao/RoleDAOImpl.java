package com.agra.spring.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.agra.spring.model.Role;

@Repository
public class RoleDAOImpl implements RoleDAO {
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Role save(Role role) {
		sessionFactory.getCurrentSession().save(role);
		return role;
	}

	@Override
	public Role get(long id) {
		return sessionFactory.getCurrentSession().get(Role.class, id);
	}

	@Override
	public List<Role> list() {
		List<Role> list = sessionFactory.getCurrentSession().createQuery("FROM com.agra.spring.model.Role").list();
		return list;
	}

	@Override
	public void update(long id, Role role) {
		Session session = sessionFactory.getCurrentSession();
		Role oldRole = session.byId(Role.class).load(id);
		oldRole.setName(role.getName());
		oldRole.setPrivileges_management(role.isPrivileges_management());
		oldRole.setDirectors_management(role.isDirectors_management());
		oldRole.setEngineers_management(role.isEngineers_management());
		oldRole.setClients_management(role.isClients_management());
		oldRole.setProducts_management(role.isProducts_management());
		oldRole.setPrices_management(role.isPrices_management());
		oldRole.setSales_management(role.isSales_management());
		oldRole.setOrders_management(role.isOrders_management());
		session.flush();
	}

	@Override
	public void delete(long id) {
		Session session = sessionFactory.getCurrentSession();
		Role role = session.byId(Role.class).load(id);
		session.delete(role);
	}

}
