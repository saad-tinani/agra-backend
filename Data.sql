-- Roles
INSERT INTO `roles` (`id`, `name`, `privileges_management`, `users_management`, `engineers_management`, `clients_management`, `products_management`, `prices_management`, `sales_management`, `orders_management`, `creation_date`, `directors_management`) VALUES
(1, 'Administrateur', b'1', b'1', b'1', b'1', b'1', b'0', b'0', b'0', NULL, b'1'),
(2, 'Directeur Régional', b'0', b'0', b'1', b'1', b'1', b'1', b'1', b'0', NULL, b'0'),
(3, 'Ingénieur Commercial', b'0', b'0', b'0', b'1', b'1', b'0', b'0', b'1', NULL, b'0'),
(4, 'Client', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', NULL, b'0');

-- Regions
INSERT INTO `regions` (`name`) VALUES
('Tanger - Tétouan - Al Hoceima'),
('Fès - Meknès');

-- Cities
INSERT INTO `cities` (`name`, `region_id`) VALUES
('Tanger', 1),
('Tétouan', 1),
('Al Hoceima', 1),
('Fès', 2),
('Meknès', 2);

-- Users
INSERT INTO `users` (`avatar`, `username`, `password`, `email`, `phone`, `first_name`, `last_name`, `cin`, `address`, `city_id`, `zip_code`, `role_id`, `status`, `creation_date`) VALUES
(NULL, 'admin', '1450575459', 'admin@gmail.com', '0660070194', 'admin', 'admin', 'D000000', 'Avenue Hassan 2', 1, 90000, 1, b'1', '2018-12-29 09:10:38'),
(NULL, 'zohdi', '1450575459', 'karim.zohdi@gmail.com', '0660070194', 'Karim', 'Zohdi', 'D987597', 'Ismailia 2, rue 3, villa 8', 5, 50000, 2, b'1', '2018-12-29 09:17:04'),
(NULL, 'tinani', '1450575459', 'saad.tinani@gmail.com', '0648958928', 'Saad', 'Tinani', 'D34853', 'Hay Boughaz, Avenue Nassr, Nº161', 2, 93200, 2, b'1', '2018-12-29 09:19:04'),
(NULL, 'zrirak', '1450575459', 'ahmed.zrirak@gmail.com', '0645123265', 'Ahmed', 'Zrirak', 'D123456', 'Touilaa', 2, 93000, 3, b'1', '2018-12-29 09:20:42'),
(NULL, 'zarwal', '1450575459', 'faissal.zarwal@gmail.com', '0645789865', 'Faissal', 'Zarwal', 'D654512', 'Rue El Hajeb', 5, 50000, 3, b'1', '2018-12-29 09:22:08');

-- Directors
INSERT INTO `directors` (`user_id`, `work_region_id`) VALUES
(2, 2),
(3, 1);

-- Engineers
INSERT INTO `engineers` (`user_id`, `work_region_id`, `work_cities_ids`) VALUES
(4, 1, '2,1'),
(5, 2, '5');

-- Categories
INSERT INTO `categories` (`description`, `name`, `position`, `status`) VALUES
('Ceci est la catégorie des yaourts !', 'Yaourt', 1, b'1');

-- Products
INSERT INTO `products` (`creation_date`, `description`, `name`, `picture`, `quantity`, `status`, `unit_price`, `category_id`, `region_id`) VALUES
('2018-12-28 13:21:45', 'Ceci est un yaourt velouté très délicieux !', 'Danone Velouté', 'product.png', 10, b'1', 2, 1, 1),
('2018-12-28 22:25:44', 'Ceci est un danette !', 'Danette', 'product.png', 20, b'1', 3.5, 1, 2);